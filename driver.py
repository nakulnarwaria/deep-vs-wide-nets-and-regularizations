'''
Created on Oct 21, 2017

@author: mroch

'''

from mydsp.pca import PCA
from mydsp.utils import pca_analysis_of_spectra
from mydsp.utils import get_corpus, get_class, Timer
from mydsp.utils import fixed_len_spectrogram, spectrogram, plot_matrix

from mydsp.features import extract_features_from_corpus
from myclassifier.feedforward import CrossValidator

from keras.layers import Dense, Dropout
from keras import regularizers
from keras.utils import np_utils

import numpy as np
import matplotlib.pyplot as plt


def main():
    files = get_corpus("/Users/nakulnarwaria/Downloads/wav/train")

    test_data = get_corpus("/Users/nakulnarwaria/Downloads/wav/test")

    # for developing
    if True:
        truncate_to_N = 50
        print("Truncating t %d files" % (truncate_to_N))
        files[truncate_to_N:] = []  # truncate test for speed
        test_data[truncate_to_N:] = []  # truncate test for speed

    print("%d files" % (len(files)))

    adv_ms = 10
    len_ms = 20

    # We want to retain offset_s about the center
    offset_s = 0.25

    # Pca analysis on training data
    pca = pca_analysis_of_spectra(files, adv_ms, len_ms, offset_s)
    components = 30

    features = extract_features_from_corpus(files, adv_ms, len_ms, offset_s, pca=pca, pca_axes_N=components)

    # Number of output categories
    labels = get_class(files)
    output_categories = len(set(labels))

    # Specify model architectures
    model_list = [

        # Wide network
        [(Dense, [40], {'activation': 'relu', 'input_dim': features.shape[1]}),
         (Dense, [40], {'activation': 'relu', 'input_dim': 40}),
         (Dense, [40], {'activation': 'relu', 'input_dim': 40}),
         (Dense, [output_categories], {'activation': 'softmax'})
         ],

        # Wide network with L1
        [(Dense, [40], {'activation': 'relu', 'input_dim': features.shape[1]}),
         (Dense, [40], {'activation': 'relu', 'kernel_regularizer': regularizers.l1(0.02)}),
         (Dense, [40], {'activation': 'relu', 'kernel_regularizer': regularizers.l1(0.02)}),
         (Dense, [output_categories], {'activation': 'softmax'})
         ],

        # Wide network with L2
        [(Dense, [40], {'activation': 'relu', 'input_dim': features.shape[1]}),
         (Dense, [40], {'activation': 'relu', 'kernel_regularizer': regularizers.l2(0.02)}),
         (Dense, [40], {'activation': 'relu', 'kernel_regularizer': regularizers.l2(0.02)}),
         (Dense, [output_categories], {'activation': 'softmax'})
         ],

        # Deep network
        [(Dense, [20], {'activation': 'relu', 'input_dim': features.shape[1]}),
         (Dense, [20], {'activation': 'relu', 'input_dim': 20}),
         (Dense, [20], {'activation': 'relu', 'input_dim': 20}),
         (Dense, [20], {'activation': 'relu', 'input_dim': 20}),
         (Dense, [20], {'activation': 'relu', 'input_dim': 20}),
         (Dense, [output_categories], {'activation': 'softmax'})
         ],

        # Deep network with L1
        [(Dense, [20], {'activation': 'relu', 'input_dim': features.shape[1]}),
         (Dense, [20], {'activation': 'relu', 'kernel_regularizer': regularizers.l1(0.02)}),
         (Dense, [20], {'activation': 'relu', 'kernel_regularizer': regularizers.l1(0.02)}),
         (Dense, [20], {'activation': 'relu', 'kernel_regularizer': regularizers.l1(0.02)}),
         (Dense, [20], {'activation': 'relu', 'kernel_regularizer': regularizers.l1(0.02)}),
         (Dense, [output_categories], {'activation': 'softmax'})
         ],

        # Deep network with L2
        [(Dense, [20], {'activation': 'relu', 'input_dim': features.shape[1]}),
         (Dense, [20], {'activation': 'relu', 'kernel_regularizer': regularizers.l2(0.02)}),
         (Dense, [20], {'activation': 'relu', 'kernel_regularizer': regularizers.l2(0.02)}),
         (Dense, [20], {'activation': 'relu', 'kernel_regularizer': regularizers.l2(0.02)}),
         (Dense, [20], {'activation': 'relu', 'kernel_regularizer': regularizers.l2(0.02)}),
         (Dense, [output_categories], {'activation': 'softmax'})
         ],

        # Deep network with dropout
        # Deep network
        [(Dropout, [0.2], {'input_shape': (features.shape[1],), 'noise_shape': None}),
         (Dense, [20], {'activation': 'relu'}),
         (Dense, [20], {'activation': 'relu', 'input_dim': 20}),
         (Dense, [20], {'activation': 'relu', 'input_dim': 20}),
         (Dense, [20], {'activation': 'relu', 'input_dim': 20}),
         (Dense, [output_categories], {'activation': 'softmax'})
         ],

        # Deep network with dropout and L2
        # Deep network
        [(Dropout, [0.2], {'input_shape': (features.shape[1],), 'noise_shape': None}),
         (Dense, [20], {'activation': 'relu', 'kernel_regularizer': regularizers.l2(0.02)}),
         (Dense, [20], {'activation': 'relu', 'kernel_regularizer': regularizers.l2(0.02)}),
         (Dense, [20], {'activation': 'relu', 'kernel_regularizer': regularizers.l2(0.02)}),
         (Dense, [20], {'activation': 'relu', 'kernel_regularizer': regularizers.l2(0.02)}),
         (Dense, [output_categories], {'activation': 'softmax'})
         ]
    ]

    trained_architectures = []
    for architecture in model_list:
        trained_architectures.append(CrossValidator(features, labels, architecture, epochs=100))

    mean_error = []
    std_deviation_error = []
    fold_count = 1

    for architecture in trained_architectures:
        mean_error.append(np.mean(architecture.get_errors()))
        std_deviation_error.append(np.std(architecture.get_errors()))

    # Extracting test data
    test_features = extract_features_from_corpus(test_data, adv_ms, len_ms, offset_s, pca=pca, pca_axes_N=30)

    test_data_labels = get_class(test_data)
    onehotlabels1 = np_utils.to_categorical(test_data_labels)

    ind = 0
    # Evaluating all folds of models
    for model in trained_architectures:
        print("Testing acc for architecture {}".format(ind))
        ind = ind + 1
        mean = 0
        for i in model.get_models():
            prediction = i.evaluate(test_features, onehotlabels1, verbose=0)
            mean = mean + float(1 - prediction[1])
            fold_count = fold_count + 1
        print("Error - {}".format(mean / 10))

    # Plot comparison graph of mean error and deviation from mean for models
    plt.errorbar([1, 2, 3, 4, 5, 6, 7, 8], mean_error, yerr=std_deviation_error,
                 label='Error deviation of all the models', fmt="o")
    plt.xlabel('Models')
    plt.ylabel('Error Rate')
    plt.legend()
    plt.show()


if __name__ == '__main__':
    plt.ion()
    main()
