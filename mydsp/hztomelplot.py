import matplotlib.pyplot as plt
import numpy as np

def plot_Hz_to_Mel():

    hz_axis = np.arange(start= 20, stop = 10000)
    mel_axis = 2595*np.log10(1 + (hz_axis/700))

    plt.figure()
    plt.plot(hz_axis, mel_axis)
    plt.xscale("log")
    plt.xlabel("Hz")
    plt.ylabel("Mel")
    plt.title("Relationship between Mel and Hz")
    plt.show()


if __name__ == '__main__':
    plt.ion()
    plot_Hz_to_Mel()
