## Abstract 
---

The purpose of this project is to explore different model architecture and regularization. In this project, **MIT/TIDIGITS corpus (Leonard and Doddington, 1993)** will be used. Make sure to download the dataset and set appropriate path to files in the driver code. This project includes previous code/functionalities provided in PCA-Analyis project, and following additional functionalities are implemented as part of the skeleton code provided beforehand by **Professor Marie A. Roch** -  

* Automatic caching of the PCA analysis and feature generation, saving time after the first run. These files are not compatible across machine architectures.
* A data structure that will allow you to specify keras network architectures. This will allow you to use the same training and code, passing in a model architecture.
* The DFTStream class has new features to support Mel filterbank processing. Using a specfmt of “Mel” will generate the output of Mel filters. The constructor allows one to specify how many Mel filters are used. The librosa package is required for the Mel filterbank construction, see DFTStream for instructions on how to install it.
* utils.get_class(file_list) uses regular expressions to identify the digit class from filenames. Use this to pull out labels from a list of files.

Dataset can be foud [here](https://catalog.ldc.upenn.edu/LDC93S10).


Following implementations are added to explore aforementioned topics - 

* Generation of fixed length spectrograms from endpointed speech, writte in mydsp.utils.fixed_len_spectrogram. The class Endpointer will run speech detection from an RMS stream and help to find the frames associated with speech. 
* Generation of features: mydsp.features.extract_features_from_corpus(). Given a set of files, generates fixed length features from a spectrogram. Returns a matrix of feature vectors for a corpus.
* myclassifier.feedforward.CrossValidator – Class for performing cross validation. Constructor should set up the test using scipy’s StratifiedKFold class and iterate across the folds. For each fold invoke the train_and_evaluate__model which trains a Keras model with K-1 folds and tests on the last fold. 

Following models are explored in this experiment which can be found in driver.py - 

```
        # Wide network
        [(Dense, [40], {'activation': 'relu', 'input_dim': features.shape[1]}),
         (Dense, [40], {'activation': 'relu', 'input_dim': 40}),
         (Dense, [40], {'activation': 'relu', 'input_dim': 40}),
         (Dense, [output_categories], {'activation': 'softmax'})
         ],

        # Wide network with L1
        [(Dense, [40], {'activation': 'relu', 'input_dim': features.shape[1]}),
         (Dense, [40], {'activation': 'relu', 'kernel_regularizer': regularizers.l1(0.02)}),
         (Dense, [40], {'activation': 'relu', 'kernel_regularizer': regularizers.l1(0.02)}),
         (Dense, [output_categories], {'activation': 'softmax'})
         ],

        # Wide network with L2
        [(Dense, [40], {'activation': 'relu', 'input_dim': features.shape[1]}),
         (Dense, [40], {'activation': 'relu', 'kernel_regularizer': regularizers.l2(0.02)}),
         (Dense, [40], {'activation': 'relu', 'kernel_regularizer': regularizers.l2(0.02)}),
         (Dense, [output_categories], {'activation': 'softmax'})
         ],

        # Deep network
        [(Dense, [20], {'activation': 'relu', 'input_dim': features.shape[1]}),
         (Dense, [20], {'activation': 'relu', 'input_dim': 20}),
         (Dense, [20], {'activation': 'relu', 'input_dim': 20}),
         (Dense, [20], {'activation': 'relu', 'input_dim': 20}),
         (Dense, [20], {'activation': 'relu', 'input_dim': 20}),
         (Dense, [output_categories], {'activation': 'softmax'})
         ],

        # Deep network with L1
        [(Dense, [20], {'activation': 'relu', 'input_dim': features.shape[1]}),
         (Dense, [20], {'activation': 'relu', 'kernel_regularizer': regularizers.l1(0.02)}),
         (Dense, [20], {'activation': 'relu', 'kernel_regularizer': regularizers.l1(0.02)}),
         (Dense, [20], {'activation': 'relu', 'kernel_regularizer': regularizers.l1(0.02)}),
         (Dense, [20], {'activation': 'relu', 'kernel_regularizer': regularizers.l1(0.02)}),
         (Dense, [output_categories], {'activation': 'softmax'})
         ],

        # Deep network with L2
        [(Dense, [20], {'activation': 'relu', 'input_dim': features.shape[1]}),
         (Dense, [20], {'activation': 'relu', 'kernel_regularizer': regularizers.l2(0.02)}),
         (Dense, [20], {'activation': 'relu', 'kernel_regularizer': regularizers.l2(0.02)}),
         (Dense, [20], {'activation': 'relu', 'kernel_regularizer': regularizers.l2(0.02)}),
         (Dense, [20], {'activation': 'relu', 'kernel_regularizer': regularizers.l2(0.02)}),
         (Dense, [output_categories], {'activation': 'softmax'})
         ],

        # Deep network with dropout
        # Deep network
        [(Dropout, [0.2], {'input_shape': (features.shape[1],), 'noise_shape': None}),
         (Dense, [20], {'activation': 'relu'}),
         (Dense, [20], {'activation': 'relu', 'input_dim': 20}),
         (Dense, [20], {'activation': 'relu', 'input_dim': 20}),
         (Dense, [20], {'activation': 'relu', 'input_dim': 20}),
         (Dense, [output_categories], {'activation': 'softmax'})
         ],

        # Deep network with dropout and L2
        # Deep network
        [(Dropout, [0.2], {'input_shape': (features.shape[1],), 'noise_shape': None}),
         (Dense, [20], {'activation': 'relu', 'kernel_regularizer': regularizers.l2(0.02)}),
         (Dense, [20], {'activation': 'relu', 'kernel_regularizer': regularizers.l2(0.02)}),
         (Dense, [20], {'activation': 'relu', 'kernel_regularizer': regularizers.l2(0.02)}),
         (Dense, [20], {'activation': 'relu', 'kernel_regularizer': regularizers.l2(0.02)}),
         (Dense, [output_categories], {'activation': 'softmax'})
         ]

```

## Results - 
---

A detailed report on the findings is attached in the project.

## Credits - 

Code skeleton provided by **Professor Marie A. Roch** (San Diego State University).